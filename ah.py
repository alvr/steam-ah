import user_games

from flask import Flask, render_template, request
from flask_caching import Cache
from steam_convert import SteamConvert

app = Flask(__name__)
app.secret_key = '3e5ae07ca859f5ad3faf8a65f4ed4ad5ee95e1a15cbf4e9f'
cache = Cache(app, config={'CACHE_TYPE': 'simple'})


@app.route('/')
@cache.cached(timeout=2 * 60 * 60)
def home():
    return render_template('index.html')


@app.route('/achievements', methods=['POST'])
def achivements():
    user = request.form['user']
    apikey = request.form['apikey']

    init = SteamConvert(user.strip().lower())
    user = init.convert().strip()
    apikey = apikey.strip()

    result = retrieve(user, apikey)

    return render_template('results.html', result=result, profile_name=init.profile_name())


@cache.memoize(timeout=60 * 60)
def retrieve(user, apikey):
    return user_games.get(user, apikey)


if __name__ == '__main__':
    app.run()
