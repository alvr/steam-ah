import re
import urllib.request
import xml.etree.ElementTree as ElementTree


class SteamConvert:
    __custom = "http://steamcommunity.com/id/{USER}/?xml=1"
    __profile = "http://steamcommunity.com/profiles/{USER}/?xml=1"
    __xml = ''

    def __init__(self, id64):
        if re.match("^\d*$", id64):
            data = urllib.request.urlopen(re.sub("{USER}", str(id64), self.__profile)).read().decode("utf8")
            self.__xml = ElementTree.fromstring(data)
        else:
            data = urllib.request.urlopen(re.sub("{USER}", id64, self.__custom)).read().decode("utf8")
            self.__xml = ElementTree.fromstring(data)

    def convert(self):
        return self.__xml[0].text

    def profile_name(self):
        return self.__xml[1].text
