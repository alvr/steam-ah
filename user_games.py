import json
import re
import requests

__owned = "http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key={API}&steamid={USER}"
__achievements = "http://api.steampowered.com/ISteamUserStats/GetPlayerAchievements/v0001/?appid={GAME}&key={" \
                 "API}&steamid={USER} "

__headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                  'Chrome/55.0.2883.87 Safari/537.36'
}


def __owned_games(id64, key):
    games = []
    url = re.sub("{USER}", id64, __owned)
    url = re.sub("{API}", key, url)
    data = requests.get(url, headers=__headers)

    try:
        doc = json.loads(data.text)
    except json.JSONDecodeError:
        return []

    for game in doc["response"]["games"]:
        games.append(game["appid"])

    return sorted(games, key=int)


def get(id64, key):
    user_games = __owned_games(id64, key)

    total_locked = 0
    total_unlocked = 0

    game_list = []
    for game in user_games:
        url = re.sub("{USER}", id64, __achievements)
        url = re.sub("{API}", key, url)
        url = re.sub("{GAME}", str(game), url)

        data = requests.get(url, headers=__headers)
        try:
            doc = json.loads(data.text)
        except json.JSONDecodeError:
            return {}

        if doc["playerstats"]["success"]:
            locked = 0
            unlocked = 0
            try:
                for achievement in doc["playerstats"]["achievements"]:
                    if int(achievement["achieved"]) == 1:
                        unlocked += 1
                    else:
                        locked += 1
            except KeyError:
                pass

            game_list.append({'name': doc["playerstats"]["gameName"],
                              'locked': locked,
                              'unlocked': unlocked,
                              'id': game})

            total_locked += locked
            total_unlocked += unlocked

    game_list = sorted(game_list, key=lambda k: k['locked'], reverse=True)

    games = {'games': game_list,
             'total_games': len(game_list),
             'total_locked': total_locked,
             'total_unlocked': total_unlocked}

    return games
